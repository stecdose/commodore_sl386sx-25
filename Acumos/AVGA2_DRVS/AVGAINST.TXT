Installing the AVGA2 Card

Tools you may need
------------------
To install your AVGA2 Card, you may need some or all of the following tools:
o  medium size flat blade screwdriver,
o  medium sized phillips head screwdriver,
o  a 3/16" nutdriver or wrench.

Prepare your AVGA2 Card
-----------------------
Your AVGA2 may or may not be shipped with the zero-wait-state jumper.  If so, 
the zero-wait-state jumper will be set to not assert the zero wait-state 
line.  You may achieve better performance in some systems by installing this 
jumper.  (Refer to the enclosed sheet entitled "Layout" for details.)

Open the Computer System
------------------------
Before you open your system, be sure that you have turned off your system 
unit and all devices connected to it.  Disconnect the cables from the back of 
the system in order to give yourself more room to work.   Note how all cables 
are connected prior to disconnection.

In a typical IBM AT compatible computer all cover mounting screws are 
located on the back of the computer.  Remove these screws.

Carefully slide the system unit cover forward.  When the cover will go no 
further, tilt it up and lift it away.

PC, XT and Compatible Users
---------------------------
DIP switches tell the computer about the various options and accessories 
installed in the system.  Successful installation of your AVGA2 Card depends 
upon the proper setting of these switches.  Before you change any switch 
settings in your computer, please jot down the current settings.  If you run 
into any difficulties, you can restore your system to its previous state.

In the IBM PC, XT and most compatible computers, levers 5 and 6 of Switch 1 
on the system board control the functions that affect the operation of your 
VGA card.  Levers 5 and 6 of Switch 1 tell the system what video adapter card 
to expect.  When your VGA card is installed, set both levers 5 and 6 of 
Switch 1 to the ON position.  This tells the computer system to expect a 
video board with a BIOS ROM such as your AVGA2 Card. Some compatible systems 
will refer to this setting as "EGA" or "reserved".

IBM AT and AT Compatible System Users
-------------------------------------
The IBM AT, IBM XT-286 and compatible computers use a program typically 
called SETUP to tell the computer what equipment is installed in your system.  
The SETUP program is usually part of the package of programs provided with 
your AT or AT compatible computer.  Your Guide To Operations (or equivalent 
manual) will explain the use of the SETUP program.  Proceed to the physical 
installation of your VGA card, then follow the instructions for using the 
SETUP program.

Your system is now ready to receive the AVGA2.  If you wish to install the 
AVGA2 in a two monitor system, please refer to Appendix C of the AVGAREAD.TXT 
file for instructions and information on limitations.  Choose an appropriate 
Expansion Slot for the AVGA2 Card.  If your system has an available 16-bit 
slot, use it.  Otherwise, choose an available 8-bit expansion slot.

Install the AVGA2 Card in your Computer
---------------------------------------
o  Check that the power is turned off.

o  If necessary, remove a system expansion slot cover by removing its 
retaining screw and lifting it out.  Save the screw.

o  Hold the AVGA2 Card by its top corners and slide it into the system unit.  
Make sure that the AVGA2 Card is correctly seated in the expansion slot.

o  Secure the AVGA2 Card with the screw that you removed in the step above.

o  Did you properly set your computer's system switches and the jumper on 
your AVGA2 Card?

o  Replace and secure the system cover.

o  Plug your VGA compatible or multi-frequency display into the 15 pin "D" 
shaped video connector at the back of the AVGA2 Card.

Important note: If you are using a multi-frequency monitor, you may need 
a special 15 pin PS/2 compatible adapter cable, and you will need to make 
sure that your multi-frequency monitor is configured properly for a PS/2 
compatible analog VGA signal.  Many multi-frequency displays have a switch 
to select "TTL" or "analog" operation; set this switch for "analog".  Check 
with your display equipment dealer or display manufacturer for additional 
information on configuring your monitor.

IBM AT and AT Compatible Users
------------------------------
After you have physically installed your AVGA2 Card in your AT compatible 
system, use the SETUP program supplied with your system to tell your 
computer about the equipment installed in your system.  When using AT and 
compatible SETUP programs with the AVGA2 Card, please follow these steps.

Run SETUP to configure your system.  Sometimes this utility comes on a 
disk, sometimes it is built in to your system's ROM BIOS.  When you arrive 
at the part of the program where video support is specified, the program 
may ask you if the monitor you are looking at will be the primary monitor.  
Answer, "YES".  If you are presented with a list of video options, select 
the option for "VGA".

If no option for VGA is listed, select support for EGA.  The AVGA2 Card 
will look to many systems like an EGA card.  If you don't see an option for 
"EGA", try the option for "NO MONITOR", "RESERVED" or "SPECIAL".  These 
will usually work.  If not, see your equipment dealer.  At the conclusion 
of the SETUP program, the computer will restart as if you had just turned 
on the power.

Very Early IBM PC's
-------------------
The ROM BIOS in very early IBM Personal Computers manufactured by IBM in 
1981 or 1982 does not have the ability to recognize the presence of an 
advanced video display adapter such as the AVGA2 card.  If you install 
your AVGA2 card in such a system, it will not function until you have 
upgraded your computer's system board BIOS ROM chips to a new version.  
BIOS ROM upgrades for the older PC model are available through IBM's 
authorized dealer network.

The most readily apparent distinguishing feature of this early model is 
that it has a main system board memory capacity of 64KB as opposed to the 
256KB, 640KB or 1 Mbyte that is standard on newer machines.  If your system 
was manufactured before October 27, 1982, you will need to see your dealer 
to arrange an upgrade of the BIOS ROM chips with a more current version.

Operation
---------
If you have followed the installation instructions in this manual, you are 
now ready to use your AVGA2 equipped computer system.  Most software that 
is compatible with IBM's Personal System/2, VGA or EGA will run automatically 
on your system using the AVGA2 Card. Just turn on your computer system and 
install your application package for "PS/2 model 50, 60 or 80 video", "VGA" 
or "EGA" as instructed by the software manufacturer.

Decompressing the AVGA2 Drivers/Utilities Diskette Files
--------------------------------------------------------
Run the INSTALL.BAT program to decompress the AVGA2 Drivers/Utilities
Diskette files to a designated drive.  Typing INSTALL by itself will display 
its usage.  For examples on running INSTALL, see the README file via the 
AVGAUTIL program.  Once in the AVGAUTIL program, click on VIEW and FILE and
type README in the box to view the file.  These files reside in the root 
directory of the AVGA2 Drivers/Utilities Diskette.

Driver Installation
-------------------
Run the AVGAUTIL.EXE program from your AVGA2 Drivers/Utilities Diskette for 
README instructions on installing or copying the AVGA2-supported drivers and 
utilities.

AVGA2 User's Guide on a Disk
----------------------------
The text file AVGAREAD.TXT is equivalent to an AVGA2 User's Guide.  This file
can be found in the AVGA2 Drivers/Utilities Diskette and can be read using the 
AVGAUTIL.EXE program.  This file gives additional information on the AVGA2 
Card.

FCC Information and Installation Files
--------------------------------------
The word processing format used for the attached FCC and Installation 
sheets was that of Word for Windows.  Both FCC.DOC and AVGAINST.DOC files can 
be found in the AVGA2 Drivers/Utilities Diskette.  The same files, saved in 
ASCII format (with a .TXT extension), can be read using the AVGAUTIL.EXE 
program.

