Note:  If you have not already done so, make sure that you run the 
AVGA2 Drivers/Utilities INSTALL program to decompress \GEMVP to a
designated disk drive.  For example:

	install c:\avga2 gemvp


AVGA2 GEM & Ventura Publisher (VP) graphics driver support files:
----------------------------------------------------------------

	SDA8600.VGA - 800x600 16 color driver file
	SDA1024.VGA - 1024x768 16 color driver file


Installing the AVGA2 Display Driver for GEM/VP:
----------------------------------------------

	Installing GEM:

	1)      Install GEM as described in the documentation
		included with the software.  If GEM is already
		installed on your system, go to Step 2.

	2)      Go to the subdirectory where GEM is installed.
		For example:  
		
		C:\> cd\gemapps\gemsys

	3)      Delete any other screen driver files with the
		.VGA extension from the \GEMAPPS\GEMSYS directory.
		
		C:\>GEMAPPS\GEMSYS> del *.vga

	4)      Copy one of the GEM/VP drivers listed above from
		the designated C:\AVGA2\GEMVP drive (the drive and 
		directory you decompressed the \GEMVP files to) to 
		the C:\GEMAPPS\GEMSYS directory.

		C:\GEMAPPS\GEMSYS> copy c:\gemvp\sda1024.vga

	5)      To ensure that the mouse will work correctly, you
		must run the GEM installation program to reset the
		mouse type.  Do the following:

		a)  Get the original GEM distribution disks and put
		    the one labeled "SYSTEM MASTER" into drive A.
		    Close the door, and goto A by typing "A:".

		b)  Run the installation program by typing
		    "GEMSETUP".

		c)  When prompted, choose 'Change existing
		    configuration', 'Continue', 'Change current
		    setup', and then select the mouse entry.  Choose
		    the entry from the list that corresponds to your
		    mouse, and reset the mouse even if the setup
		    looks correct.

		d)  Answer the rest of the questions, saving the
		    new setup.

	6)      Run the application as before, by typing GEM.

		Example:  C:\> gem

WARNING:  In the 1024x768x16 resolution, GRAPH.APP will consistently
	  fail causing garbage to appear, the graph drawing to lose
	  its contents, the horizontal lines in the spreadsheet to
	  disappear, and when exiting Graph, the mouse to lockup.
	  These symptoms were observed in other Video Cards in the
	  same resolution.  This is caused by GEM GRAPH's memory
	  limitation in this resolution.


Installing AVGA2 Display driver for Ventura Publisher 2.0:
---------------------------------------------------------

	1)      Install VP as described in the documentation
		included with the software.  If VP is already
		installed on your system, go to Step 2.

	2)      Go to the subdirectory where VP is installed.

		Example:  C:\> cd\ventura

	3)      Copy both driver files listed above from the
		designated C:\AVGA2\GEMVP drive (the drive and 
		directory you decompressed the \GEMVP files to) 
		to your hard disk directory C:\>VENTURA>.

		Example:  C:\VENTURA> copy c:\avga2\gemvp\*.vga

	4)      Use any text editor to modify the batch file used
		to run VP.  The file is in the root directory, and
		named VP.BAT or VPPROF.BAT.  Change the entry for
		the screen driver to use the AVGA2 driver.  For
		example,

		  Change the line that reads:

		    DRVRMRGR %1 /S=SDFVGAH5.VGA/M=01

		  To read:

		    DRVRMRGR %1 /S=SDA8600.VGA/M=01

		  for the 800x600x16 resolution, or

		    DRVRMRGR %1 /S=SDA1024.VGA/M=01

		  for the 1024x768x16 resolution.

	5)      Run the application as before, by typing VP or VPPROF.

		Example:  C:\> vp

